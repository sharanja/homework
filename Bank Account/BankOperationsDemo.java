package prog.bcas.bank;

public class BankOperationsDemo {

	public static void main(String[] args) {

		BankOperations accSharanja = new BankOperations();
		BankOperations accKannathasan = new BankOperations();

		accSharanja.openAccount("Jaffna", "Savings Account", "123456789", "K.Sharanja", 100000);
		accKannathasan.openAccount("Jaffna", "Savings Account", "987654321", "S.Kannathasan", 200000);

		accSharanja.getAccountDetails("123456789");
		accKannathasan.getAccountDetails("987654321");
		
		accSharanja.withdrawal(15000);
		accKannathasan.deposit(70000);

		accSharanja.getAccBalance();
		accKannathasan.getAccBalance();
		
		System.out.println("K.Sharanja account balance = "+ accSharanja.getAccBalance());
		System.out.println();
		System.out.println("S.Kannathasan account balance = "+ accKannathasan.getAccBalance());
		
	}
}
