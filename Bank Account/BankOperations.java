package prog.bcas.bank;

public class BankOperations {
	
	public static final String bankName = "Commercial Bank";
	private double accBalance;
	private String accName;
	private String accNumber;
	private String accHolderName;
	private String branch;
	
	
public void openAccount(String branch, String accName, String accNumber, String accHolderName, int amount){
	
	this.branch = branch;
	this.accName = accName;
	this.accNumber = accNumber;
	this.accHolderName = accHolderName;
	this.accBalance = amount;
}
	
public void withdrawal(double amount){
		accBalance = accBalance - amount;
	}
	
public void deposit(double amount){
		accBalance = accBalance + amount;
	}

public double getAccBalance(){
	return accBalance;
	}

public void getAccountDetails(String accNumber) {
		System.out.println("_______________________________________________________");
		System.out.println("Bank Name : " + bankName + "-" + branch);
		System.out.println("Account Number : " + accNumber);
		System.out.println("Account Name : " + accName);
		System.out.println("Account Holder Name : " + accHolderName);
		System.out.println("Account Balance : " + accBalance);
		System.out.println("_______________________________________________________");
		System.out.println();
	}
}
