public class SumOfNaturalNumbers{

    public static void main(String[] args) {

       int num = 10;
	   int count= 1;
       int total = 0;

       for(count = 1; count <= num; count++){
           total = total + count;
       }

       System.out.println("Sum of first 10 natural numbers is: "+total);
    }
}